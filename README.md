<p align="center"><a href="https://iutnantes.univ-nantes.fr/"><img src="https://iutnantes.univ-nantes.fr/medias/photo/logoiutq_1377690591795.gif?ID_FICHE=627306" width="150"></a></p>

# Application de Demandes de Bourses (ADB)

## À propos

Ce projet est réalisé par des élèves de l'[IUT de Nantes](https://iutnantes.univ-nantes.fr/) pour le compte d’[ATALA](https://www.atala.org/) (Association pour le Traitement Automatique des LAngues) qui est une association scientifique Française. Le but consiste à faciliter le travail de gestion des demandes de bourses l’association. Il possède deux objectifs, le premier est de centraliser les informations et le second est de permettre un gain de temps sur la gestion de la trésorerie. Si le projet concerne ATALA en premier lieu, il doit, dans un second temps, être pensé pour être utilisé par d’autres associations.

## Documentations

### Framework
- [Lumen 6](https://lumen.laravel.com/docs/6.x)

## Utilisation

### Pré-requis

- [Projet AwA](https://gitlab.com/ebam-devteam/application-web-d-administration)
    - [PHP](https://www.php.net/)
    - [Composer](https://getcomposer.org/)
    - [MySQL](https://www.mysql.com/)
    - [Apache2](https://doc.ubuntu-fr.org/apache2) (optionnel)

__N.B.__ Pour Windows, il est très conseillé d'installer [WAMP](http://www.wampserver.com/) qui regroupe déjà Apache, MySQL et PHP.

### Installation

1. Télécharger ou fork le projet depuis https://gitlab.com/ebam-devteam/application-demande-de-bourse
2. Exécuter `composer install`
3. Copier le `.env.example.prod` ou `.env.example.dev` (selon les besoins) en `.env`
4. Configurer le `.env` selon votre configuration système
5. Démarrer un serveur PHP depuis `/public`

## License

Ce projet est sous licence MIT - voir le fichier [LICENCE](https://gitlab.com/ebam-devteam/application-demande-de-bourse/-/blob/master/LICENSE) pour plus de détails.
