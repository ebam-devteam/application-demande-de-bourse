# Changelog

#### **Version 1.0.1** Unreleased on April 17th 2020

- Added comments ScholarshipController
- Added comments AuthController
- Added comments web



#### **Version 1.0.0** Released on April 17th 2020

First public release, only major additions are listed.
- Added registration
- Added login
- Added scholarship application
  - Added file upload
