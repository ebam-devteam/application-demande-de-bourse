<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


//if you want to know the lumen's version
/*$router->get('/', function () use ($router) {
    return $router->app->version();
});*/

$router->get('/', function(){
    return view('home');
} );
$router->post('auth','AuthController@authVerify');
$router->get('logout','AuthController@logout');
//Only if is autenticated
$router->get('page/{id}', ['middleware' => 'authenticate','uses' =>'ScholarshipController@show']);

$router->get('register', 'ScholarshipController@create');
$router->post('register/store', 'ScholarshipController@store');

$router->get('sholarship/edit/{id}', ['middleware' => 'authenticate','uses' =>'ScholarshipController@edit']);
$router->post('user/update/{id}', ['middleware' => 'authenticate','uses' =>'ScholarshipController@update']);

$router->get('sholarship/infoSpends/{id}', ['middleware' => 'authenticate','uses' =>'ScholarshipController@editSpends']);
$router->post('scholarship/updateSpends/{id}', ['middleware' => 'authenticate','uses' =>'ScholarshipController@updateSpends']);


