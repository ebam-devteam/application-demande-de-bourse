<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Validator;


/**
 * Controller who make the autentication
 */
class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * request : the request using the post method
     * verify if the connexion is correct
     */
    public function authVerify(Request $request){
        
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return view('home', ["errors" => $v->errors()]);
        }

        $user = DB::table('scholarship')->where('email',($request['email']))->first();
        if ($user != null && password_verify($request['password'], $user->password)) {
            $_SESSION['auth'] = true;
            return redirect('page/'.$user->id);
        }else {
            return redirect('/');
        }
    }

    /**
     * to deconnect
     */
    public function logout(Request $request){
        session_destroy();
        return redirect('/');
    }
}