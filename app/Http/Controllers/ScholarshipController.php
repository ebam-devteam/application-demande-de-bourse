<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Validator;


class ScholarshipController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * id : id of the shcolarship
     * return the view call user
     */
    public function show($id){

        $userFiles = DB::table('scholarship')
            ->join('files','scholarship.id','=', 'files.scholarship_id')
            ->where('scholarship.id','=', $id)
            ->get();
        //var_dump($user);exit();
        $user = $userFiles[0];
        $user->id = $id;

        $user->cv = null;
        $user->transport = null;
        $user->housing = null;
        foreach($userFiles as $file){
            switch ($file->reason) {
                case 'cv':
                    $user->cv = $file->fileName;
                    break;
                case 'fileTransport':
                    $user->transport = $file->fileName;
                    break;

                case 'fileHousing':
                    $user->housing = $file->fileName;
                    break;
            }
        }

        return view("user", ['user' => $user]);
    }


    /**
     * return the view to create a scholarship
     */
    public function create(){
        return view('registerForm');
    }

    /**
     * request : the request using the post method
     * store a new scholarship in the Database
     */
    public function store(Request $request){
        //if the request is not validate
        $validator = Validator::make($request->all(), [
            'lastName' => 'required',
            'firstName' => 'required',
            'email' => 'required',
            'password' => 'required',
            'laboratory' => 'required',
            'cv' => 'required'
        ]);
        if ($validator->fails()) {
            return view('registerForm', ["errors" => $validator->errors()]);
        }
        try {
            $id = DB::table('scholarship')->insertGetId([
                'firstName' => $request['firstName'],
                'lastName' => $request['lastName'],
                'email' => $request['email'],
                'password' => password_hash($request['password'],PASSWORD_DEFAULT),
                'laboratory' => $request['laboratory']
            ]);
        } catch (\Throwable $th) {
            if ($th->getCode() =="23000") {
                $request->password = null;
                return view('registerForm', ["errors" => "L'adresse mail est déjà utilisé", "param" => $request ]);
            }
            var_dump( $th);exit();
        }


        $file = $_FILES["cv"];
        //store the file
        $this->storeFile($id, $file, "cv");
        $_SESSION['auth'] =true;
        return view('confirmation', ['userId' => $id]);
    }

    /**
     * id : id of the scholarship who is concern by the file
     * file : the file that we need to store
     * key : the role of the file
     * function to store a file in a directory of the server
     */
    public function storeFile($id, $file, $key){

        //define the path where files are strore
        $path = "../../uploads/";

        //create directory if doesnt exist
        if (!file_exists($path)) {
            mkdir ( $path, 0777);
        }
        //create directory for the if doesnt exist
        $path = $path.$id.'/';
        if (!file_exists($path)) {
            mkdir ( $path, 0777);
        }

        // File update name
        $time = microtime(false);
        $values =  preg_split("/[\s,]+/", $time);
        $extention = preg_split("/[.]+/", basename($file["name"]));
        //switch to correctly name the file
        switch ($key) {
            case 'cv':
                $name = 'CV_Lettre_de_Motivation';
                break;
            case 'fileTransport':
                $name = 'Justificatif_Transport';
                break;
            case 'fileHousing':
                $name = 'Justificatif_Logement';
                break;

            default:
                $name = $extention[0];
                break;
        }

        $fileName = $name.'_'.end($values).'.'.end($extention);
        $targetFilePath = $path . $fileName;

        // Include the database configuration file
        $statusMsg = '';


        // Take extension file
        $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);

        if(isset($_POST["submit"]) && !empty(($file["name"]))){
            // Allow certain file formats
            $allowTypes = array('pdf');
            if(in_array($fileType, $allowTypes)){
                // Upload file to server
                if(move_uploaded_file($file["tmp_name"], $targetFilePath)){
                    // Insert file name into database

                    DB::table('files')->insert([
                        'fileName' => $fileName,
                        'filePath' => $targetFilePath,
                        'Scholarship_id' => $id,
                        'reason' => $key
                    ]);
                }else{
                    $statusMsg = "Sorry, there was an error uploading your file.";
                    return view('registerForm', ["errors" => $statusMsg]);
                }
            }else{
                $statusMsg = 'Sorry, only PDF files are allowed to upload.';
                return view('registerForm', ["errors" => $statusMsg]);
            }
        }else{
            $statusMsg = 'Please select a file to upload.';
            return view('registerForm', ["errors" => $statusMsg]);
        }
    }

    /**
     * request : the request using the post method
     * id : id of the scholarship who is concern by the file
     * return the view to edit a scholarship
     */
    public function edit(Request $request, $id){
        $user = DB::table('scholarship')
            ->join('files','scholarship.id','=', 'files.scholarship_id')
            ->where('scholarship.id','=', $id)
            ->first();
        $user->id = $id;
        return view("editScholarship", ['user' => $user]);
    }

    /**
     * request : the request using the post method
     * id : id of the scholarship who is concern by the file
     * update the scholarship who have the same id by the value of the parameter request
     */
    public function update(Request $request, $id){

        $validator = Validator::make($request->all(), [
            'lastName' => 'required',
            'firstName' => 'required',
            'email' => 'required',
            'laboratory' => 'required'
        ]);

        if ($validator->fails()) {
            return view('registerForm', ["errors" => $validator->errors()]);
        }
        //make the update in the DB        
        $affected = DB::table('scholarship')
              ->where('id', $id)
              ->update([
                'firstName' => $request['firstName'],
                'lastName' => $request['lastName'],
                'email' => $request['email'],
                'laboratory' => $request['laboratory'],
          ]);
        //if the file cv was update
        if ($_FILES['cv']['name'] != '') {
             //check if the file is already exist in the DB
            $deletefile = DB::table('files')
            ->select('id', 'filePath')
            ->where('scholarship_id', $id)
            ->where('reason', 'cv')
            ->first();
            //if deletefile is null the file exist in the directory
            if ($deletefile != null && file_exists($deletefile->filePath)) {
                //delet the file in the database
                DB::table('files')->where('id', $deletefile->id)->delete();
                //delet the file in the directory
                unlink($deletefile->filePath);
            }
            $this->storeFile($id, $_FILES['cv'], 'cv');
        }

        return redirect('page/'.$id);
    }

    /**
     * request : the request using the post method
     * id : id of the scholarship who is concern by the file
     * return the view to edit spends information
     */
    public function editSpends(Request $request, $id){
        $user = DB::table('scholarship')
            ->select('whoPaid', 'refund')
            ->where('scholarship.id','=', $id)
            ->first();
        $user->id = $id;
        return view("infoSpends", ['user' => $user]);
    }

    /**
     * request : the request using the post method
     * id : id of the scholarship who is concern by the file
     * update spends information of the scholarship who have the same id
     * update file concern
     */
    public function updateSpends(Request $request, $id){

        $affected = DB::table('scholarship')
              ->where('id', $id)
              ->update([
                'whoPaid' => $request['whoPaid'] == "none" ? null : $request['whoPaid'],
                'refund' => $request['refund'] == "none" ? null : $request['refund'],
          ]);
        foreach ($_FILES as $key => $file) {


            if ($file['name'] != '') {
                $deletefile = DB::table('files')
                    ->select('id', 'filePath')
                    ->where('scholarship_id', $id)
                    ->where('reason', $key)
                    ->first();
                //delte file
                if ($deletefile != null && file_exists($deletefile->filePath)) {
                    DB::table('files')->where('id', $deletefile->id)->delete();
                    unlink($deletefile->filePath);
                }
                $this->storeFile($id, $file, $key);
            }
        }
        return view('confirmation', ['userId' => $id]);
    }

}
