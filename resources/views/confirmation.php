<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="/css/validation-creation.css" rel="stylesheet" type="text/css">
    <title>Atala - Demande de bourse</title>
</head>
<body>
    <main>
        <div class="container-content">
            <div class="text-part">
                <h2>Votre demande de bourse à bien été prise en compte</h2>
                <p>Cliquez sur le bouton suivant pour voir un récapitulatif de votre bourse.</p>
            </div>
            <div class="link-part">
                <a href="/page/<?=$userId?>">Continuer</a>
            </div>
        </div>
    </main>
</body>
</html>
