<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="/css/create-account.css" rel="stylesheet">
    <link href="/css/form.css" rel="stylesheet">
    <title>Atala - Demande de bourse</title>
</head>
<body>
    <main>
        <div class="top-part">
            <a href="/">Retour à la page de connexion</a>
            <img src="/images/atala_logo_text.png" alt="logo association">
            <h1>Demande de Bourse auprès d'ATALA</h1>
        </div>

        <div class="new-account-form">
            <form action="/register/store" method="post"  enctype="multipart/form-data">
                <div class="case_infos">
                    <label for="lastName">Nom</label>
                    <input type="text" id="lastName" name="lastName" required="required" value="<?php if(isset($param)) echo $param->lastName ?>" >
                </div>
                <div class="case_infos">
                    <label for="firstName">Prénom</label>
                    <input type="text" id="firstName" name="firstName" required="required" value="<?php if(isset($param)) echo $param->firstName ?>" >
                </div>
                <div class="case_infos">
                    <label for="email">Adresse Mail</label>
                    <input type="email" id="email" name="email" required="required" value="<?php if (isset($param)) echo $param->email?>" >
                    <?php if (isset($errors)) echo $errors;?>
                </div>
                <div class="case_infos">
                    <label for="laboratory">Laboratoire</label>
                    <input type="text" id="laboratory" name="laboratory" required="required" value="<?php if(isset($param)) echo $param->laboratory ?>" >
                </div>
                <div class="case_infos">
                    <label for="cv">Document au format PDF contenant un CV et une lettre de motivation</label>
                    <input type="file" id="cv" name="cv" required="required">
                </div>
                <div class="case_infos">
                    <label for="passord">Mot de Passe</label>
                    <input type="password" id="password" name="password" required="required">
                </div>

                <button type="submit" name="submit">Envoyer la demande</button>
            </form>
        </div>
    </main>
</body>
</html>
