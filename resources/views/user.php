<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="/css/default.css" rel="stylesheet">
    <link href="/css/your-scholarship.css" rel="stylesheet">
    <title>Atala - Demande de bourse</title>
</head>
<body>
    <main>
        <div class="container-content">
            <div class="navigation-part">
                <h1>Les informations de votre bourse</h1>
                <nav>
                    <ul>
                        <li class="selected"><a href="/page/<?=$user->id?>">Ma bourse</a></li>
                        <li><a href="/sholarship/edit/<?=$user->id?>">Editer votre bourse</a></li>
                        <li><a href="/sholarship/infoSpends/<?=$user->id?>">Information de dépense</a></li>
                        <li><a href="/logout">Déconnexion</a></li>
                    </ul>
                </nav>
            </div>

            <div class="info-user">
                <div class="container-icon">
                    <div>
                        <img src="/images/user.svg" alt="icon personne">
                    </div>
                </div>

                <div class="container-info">
                    <div class="info">
                        <p>Nom :</p>
                        <p><?= $user->lastName?></p>
                    </div>
                    <div class="info">
                        <p>Prénom :</p>
                        <p><?= $user->firstName?></p>
                    </div>
                    <div class="info">
                        <p>Adresse Mail :</p>
                        <p><?= $user->email?></p>
                    </div>
                    <div class="info">
                        <p>Laboratoire :</p>
                        <p><?= $user->laboratory?></p>
                    </div>
                </div>
            </div>

            <div class="files-container">
                <h2>Vos documents</h2>
                <div class="table-container">
                    <div class="table-row">
                        <div class="table-content" data-title="Désignation du fichier">
                            <p>CV et lettre de motivation</p>
                        </div>
                        <div class="table-content" data-title="Nom du fichier">
                            <p><?= $user->cv != null ? $user->cv : "N'est pas renseigné" ;?></p>
                        </div>
                    </div>
                    <div class="table-row">
                        <div class="table-content" data-title="Désignation du fichier">
                            <p>Justificatif moyen de transport</p>
                        </div>
                        <div class="table-content" data-title="Nom du fichier">
                            <p><?= $user->transport != null ? $user->transport : "N'est pas renseigné" ;?></p>
                        </div>
                    </div>
                    <div class="table-row">
                        <div class="table-content" data-title="Désignation du fichier">
                            <p>Justificatif moyen de logement</p>
                        </div>
                        <div class="table-content" data-title="Nom du fichier">
                            <p><?= $user->housing != null ? $user->housing : "N'est pas renseigné" ;?></p>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <footer>
            <div>
                Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik"> Freepik </a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
            </div>
        </footer>
    </main>
</body>
</html>
