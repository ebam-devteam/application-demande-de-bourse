<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Atala - Demande de bourse</title>

    <link href="css/home.css" rel="stylesheet">
    <title>Atala - Demande de Bourse</title>
    <link href="/css/home.css" rel="stylesheet">
    <title>Home</title>
</head>
<body>
    <main>
        <div class="top-part">
            <img src="images/atala_logo_text.png" alt="logo association">
            <h1>Bourse ATALA</h1>
        </div>

        <div class="bottom-part">
            <div class="connexion">
                <form action="/auth" method="post">
                    <h2>Se connecter</h2>
                    <label for="email"></label>
                    <input type="email" name="email" id="email" required="required" placeholder="Votre adresse e-mail">
                    <label for="password"></label>
                    <input type="password" name="password" id="password" required="required" placeholder="Votre mot de passe">

                    <button type="submit">Se conneter</button>
                </form>
                <?php if(isset($errors)) echo $errors ?>
            </div>

            <div class="create-account">
                <div>
                    <h2>Vous n'avez pas de compte, vous pouvez en créer un</h2>
                    <a href="/register">Faire une demande de bourse</a>
                </div>
            </div>
        </div>
    </main>
</body>
</html>
