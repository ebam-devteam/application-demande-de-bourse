<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="/css/default.css" rel="stylesheet">
    <link href="/css/form.css" rel="stylesheet">
    <title>Atala - Demande de Bourse</title>
</head>
<body>
    <main>
        <div class="container-content">
            <div class="navigation-part">
                <h1>Les informations de votre bourse</h1>
                <nav>
                    <ul>
                        <li><a href="/page/<?=$user->id?>">Ma bourse</a></li>
                        <li class="selected"><a href="/sholarship/edit/<?=$user->id?>">Editer votre bourse</a></li>
                        <li><a href="/sholarship/infoSpends/<?=$user->id?>">Information de dépense</a></li>
                        <li><a href="/logout">Déconnexion</a></li>
                    </ul>
                </nav>
            </div>

            <div class="edit-part">
                <h2>Edition de votre bourse</h2>
                <form action="/user/update/<?= $user->id?>" method="post"  enctype="multipart/form-data">
                    <div class="case_infos">
                        <label for="lastName">Nom</label>
                        <input type="text" id="lastName" name="lastName" required="required" value="<?= $user->lastName?>">
                    </div>
                    <div class="case_infos">
                        <label for="firstName">Prénom</label>
                        <input type="text" id="firstName" name="firstName" required="required"value="<?= $user->firstName?>">
                    </div>
                    <div class="case_infos">
                        <label for="email">Adresse Mail</label>
                        <input type="email" id="email" name="email" required="required"value="<?= $user->email?>">
                    </div>
                    <div class="case_infos">
                        <label for="laboratory">Laboratoire</label>
                        <input type="text" id="laboratory" name="laboratory" required="required"value="<?= $user->laboratory?>">
                    </div>
                    <div class="case_infos">
                        <label for="cv">Document au format PDF contenant un CV et une lettre de motivation</label>
                        <input type="file" id="cv" name="cv">
                        <p class="warning">Ne renseigner ce champ que si vous souhaitez changer le document</p>
                    </div class="case_infos">

                    <button type="submit" name="submit">Enregistrer les modifications</button>
                </form>
            </div>

        </div>
    </main>
</body>
</html>
