<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="/css/default.css" rel="stylesheet">
    <link href="/css/form.css" rel="stylesheet">
    <title>Document</title>
</head>
<body>
    <main>
        <div class="container-content">
            <div class="navigation-part">
                <h1>Les informations de votre bourse</h1>
                <nav>
                    <ul>
                        <li><a href="/page/<?=$user->id?>">Ma bourse</a></li>
                        <li><a href="/sholarship/edit/<?=$user->id?>">Editer votre bourse</a></li>
                        <li class="selected"><a href="/sholarship/infoSpends/<?=$user->id?>">Information de dépense</a></li>
                        <li><a href="/logout">Déconnexion</a></li>
                    </ul>
                </nav>
            </div>

            <div class="expenses-part">
                <h2>Un complément d'information est nécessaire</h2>
                <form action="/scholarship/updateSpends/<?= $user->id?>" method="post" enctype="multipart/form-data">
                    <div class="case_infos">
                        <label for="whoPaid">Sélectionnez l'entité qui a payé</label>
                        <select name="whoPaid" id="whoPaid">
                            <option value="you" <?php if($user->whoPaid == "you") echo("selected")?> >Vous même</option>
                            <option value="company" <?php if($user->whoPaid == "company") echo("selected")?> >Votre Entreprise</option>
                            <option value="laboratory" <?php if($user->whoPaid == "laboratory") echo("selected")?> >Votre Laboratoire</option>
                            <option value="none"<?php if($user->whoPaid == null) echo("selected")?> >Selectionner une option</option>
                        </select>
                    </div>
                    <div class="case_infos">
                        <label for="refund">Sélectionnez la façon dont vous voulez être rembourser</label>
                        <select name="refund" id="refund">
                            <option value="cheque" <?php if($user->refund == "cheque") echo("selected")?> >Cheque</option>
                            <option value="virement" <?php if($user->refund == "virement") echo("selected")?> >Virement Bancaire</option>
                            <option value="none" <?php if($user->refund == null) echo("selected")?> >Selectionner une option</option>
                        </select>
                    </div>

                    <div class="case_infos">
                        <label for="fileTransport">Si vous avez déjà acheté un billet permettant de vous rendre à la conférence, merci de rensiegner un justificatif</label>
                        <input type="file" id="fileTransport" name="fileTransport">
                        <p class="warning">Ne renseigner ce champ que si vous souhaitez ajouter un justificatif ou modifier celui-ci. </p>
                    </div>

                    <div class="case_infos">
                        <label for="fileHousing">Si vous avez déjà effectué une réservation, permettant de vous loger, merci de renseigner un justificatif</label>
                        <input type="file" id="fileHousing" name="fileHousing">
                        <p class="warning">Ne renseigner ce champ que si vous souhaitez ajouter un justificatif ou modifier celui-ci. </p>
                    </div>

                    <div>
                        <button type="submit" name="submit">Enregistrer les modifications</button>
                    </div>
                </form>
            </div>

        </div>
    </main>
</body>
</html>
